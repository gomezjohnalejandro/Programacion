﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Programacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class page2 : ContentPage
	{
		public page2 ()
		{
			InitializeComponent ();
		}
        async private void validationRegister(object sender, EventArgs e)
        {
            //Validar Campos Vacios
            if (string.IsNullOrEmpty(entryName.Text) || string.IsNullOrEmpty(entryLastname.Text) || string.IsNullOrEmpty(entryPhoneNumber.Text))
            {
                // Mensaje para un label
                /*labelName.TextColor = Color.Red;
                labelName.Text = "Debe escribir nombre.";
                labelSurname.Text = "Debe escribir apellidos.";
                labelCellPhoneNumber.Text = "Debe escribir numero de celular.";*/

                await DisplayAlert("Error", "Debe escribir nombre, apellidos y numero de celular.", "OK");
            }
            else
            {
                labelName.TextColor = Color.Green;
                labelLastname.TextColor = Color.Green;
                labelPhoneNumber.TextColor = Color.Green;
                labelName.Text = entryName.Text;
                labelLastname.Text = entryLastname.Text;
                labelPhoneNumber.Text = entryPhoneNumber.Text;
                await DisplayAlert("Error", " usuario  registrado .", "OK");

                //await Navigation.PushAsync(new Page1());
            }

        }
    }
}