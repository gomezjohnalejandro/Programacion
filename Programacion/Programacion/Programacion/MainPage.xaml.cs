﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Programacion
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        async private void ButtonClicked(object sender, EventArgs e)
        {
          if(String.IsNullOrEmpty(entryUser.Text) || String.IsNullOrEmpty(entryPassword.Text))
            {
                labelMessage.TextColor = Color.Red;
                labelMessage.Text = "Debe escribir Usuario y Contraseña.";

               await DisplayAlert("Error", "Debe escribir Usuario y Contraseña.", "OK");
            }
            else
            {
                labelMessage.TextColor = Color.Green;
                labelMessage.Text = "Su Sesiòn fue iniciada Correctamente";

              await   Navigation.PushAsync(new page2());
            }
        }
	}
}
